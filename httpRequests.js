

const TOKEN =
  "a5c4a78740bf54893fea58fe5b7e1012e2539f378f115d1003139092d69d181b";

function putUsers(id, data) {
  return fetch("https://gorest.co.in/public/v2/users/" + id, {
    method: "PUT",
    headers: {
      Authorization: "Bearer " + TOKEN,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then((res) => res.json());
}
putUsers(3469, {
  email: "mary@gmail.com",
  name: "Mary",
  gender: "female",
  status: "active",
}).then((res) => console.log(res));

function getUsers() {
  return fetch("https://gorest.co.in/public/v2/users", {
    method: "GET",
    headers: {
      Authorization: "Bearer " + TOKEN,
    },
  }).then((res) => res.json());
}
getUsers().then((data) => console.log(data));

// fetch("https://gorest.co.in/public/v2/posts").then(
//   response=>{
//     if(response.ok){
//        response.headers.get("content-type" === "application/json");
//        return response.json();
//     }
//     else {
//       throw new Error(`Unexpected response status ${response.status} or ${response.statusText}`);
//     }
//   }
// ).then(json => console.log(json)).then(json => console.log(json)).catch(err => console.log(err));

function postUser(user) {
  return fetch("https://gorest.co.in/public/v2/users", {
    method: "POST",
    headers: {
      Authorization: "Bearer " + TOKEN,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(user),
  })
    .then((res) => res.json())
    .then((json) => console.log(json))
    .catch((err) => console.log(err));
}
postUser({
  name: "lUCAS",
  email: "luc@mail.com",
  gender: "male",
  status: "active",
});
