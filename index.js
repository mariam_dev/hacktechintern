function deepClone(obj) {
  if (typeof obj !== "object") {
    return obj;
  }
  let newObj = {};
  for (let i in obj) {
    newObj[i] = deepClone(obj[i]);
  }
  return newObj;
}

const object1 = {
  a: "somestring",
  b: 42,
  c: false,
};
console.log(deepClone(object1));
