import React from 'react'
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useState } from 'react'
import { Heart } from './Heart';
import './shopItem.css'
import { Trash } from './Trash';
import NumberInput from './NumberInput';

export default function ShopItem({
    productImg,
    productName,
    quantity,
    price,
    isDisable,
    initialValue,
    getQuantity,
    onDelete,
    isFavorite,
    onClickFavorite,
}) {

    const [itemValue,setItemValue]=useState(initialValue);

    useEffect(()=>{
        getQuantity(itemValue);
       },[itemValue]);

    const [checked, setChecked] = useState(isFavorite);
    
    useEffect(() => {
      setChecked(isFavorite);
    }, [isFavorite]);
    const textColor = isDisable ? '#d3d3d3' : 'black';
   
    
  return (
    <div className="shopping-item">
      <div className="product-picture">
        <div className="picture">
          <img
           src={`./assets/${productImg}`}
            alt={productName}
            width="185px"
            height="145px"
          />
        </div>

        <div className="item-settings">
          <h2>{productName}</h2>
          <p style={{ color: textColor }}>({quantity} units left)</p>
          <div className="trash">
            <button
              disabled={isDisable}
              type="button"
              onClick={onDelete}
              className="remove"
            >
              <Trash
                height="20"
                stroke={isDisable ? '#d3d3d3' : '#000000'}
                fill={isDisable ? '#d3d3d3' : '#000000'}
              />
              Remove Item
            </button>
          </div>
          <div className="favorites">
            <button
              disabled={isDisable}
              type="button"
              onClick={onClickFavorite}
              className="add"
            >
              {checked ? (
                <Heart
                  height="20"
                  fill="#8E0EF2"
                  strokeWidth="0"
                  stroke={isDisable ? '#d3d3d3' : 'black'}
                />
              ) : (
                <Heart height="20" stroke={isDisable ? '#d3d3d3' : 'black'} />
              )}
              Add to favorites
            </button>
          </div>
        </div>
      </div>
       <div className="quantity-component">
        <NumberInput
          maxAvailable={quantity}
          getQuantity={setItemValue}
          initValue={initialValue}
          disabled={isDisable}
        /> 
         <p style={{ color: textColor }}>{price} AMD</p>
      </div>
    </div>
  )
}

ShopItem.propTypes = {
    productImg: PropTypes.string.isRequired,
    productName: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    initValue: PropTypes.number,
    isDisable: PropTypes.bool,
    getQuantity: PropTypes.func,
    onDelete: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool,
  };
  
  ShopItem.defaultProps = {
    initValue: 1,
    isDisable: false,
    getQuantity: (value) => value,
    isFavorite: false,
  };

