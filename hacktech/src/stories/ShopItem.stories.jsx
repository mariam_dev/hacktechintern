import React from "react";

import ShopItem from "./ShopItem";

export default {
    title: 'components/ ShopItem',
    component: ShopItem,
}

const Template = (args) => <ShopItem {...args} />;

export const Default = Template.bind({});

Default.args={
    productName: "Product Name",
    quantity:  20,
    price:  1000,
    isDisable:  false,
    onDelete:() => {
      return 'click';
    },
    isFavorite:  false,
}

export const Disable = Template.bind({});

Disable.args={
    ...Default.args,
    isDisable:  true, 
    isFavorite:  true,
}

export const DisableFave = Template.bind({});

DisableFave.args={
    ...Default.args,
    isDisable:  true, 
}