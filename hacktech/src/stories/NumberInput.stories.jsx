import React from "react";
import NumberInput from "./NumberInput";

export default {
    title: 'Components/ NumberInput',
    component: NumberInput,
    argTypes: {
      getQuantity: { action: 'clicked' },
    },
    parameters: { actions: { argTypesRegex: '^on.*' } },
    
}
 const Template = (args) => <NumberInput {...args} />;

 export const QuantityChanger = Template.bind({});

QuantityChanger.args = {
  initValue: 1,
  maxAvailable: 9,
  disabled: false,
};